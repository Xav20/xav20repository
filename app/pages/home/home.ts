import {Component} from '@angular/core';
import {NavController, MenuController, Page, NavParams} from 'ionic-angular';

@Page({
  templateUrl: 'build/pages/home/home.html'
})
export class HomePage {
  constructor(private navCtrl: NavController, private menu: MenuController) {
  	this.menu.enable(true);
  }
}
